package kg.nurtelecom.ofdapp.application

import android.app.Application
import kg.nurtelecom.ofdapp.koin.appKoin
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class OfdApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        setupKoin()
    }

    private fun setupKoin() {
        startKoin {
            androidContext(this@OfdApplication)
            modules(appKoin)
        }
    }

}