package kg.nurtelecom.ofdapp.koin

import kg.nurtelecom.ofdapp.main.Config
import kg.nurtelecom.ofdapp.main.MainVM
import kg.nurtelecom.ofdapp.main.MainVmImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appKoin = module {
    viewModel<MainVM> { MainVmImpl(get()) }
    single<Config> { Config("http://www.o.kg") }
}