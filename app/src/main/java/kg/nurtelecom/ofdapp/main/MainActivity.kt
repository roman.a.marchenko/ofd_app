package kg.nurtelecom.ofdapp.main

import android.util.Log
import kg.nurtelecom.core.activity.CoreActivity
import kg.nurtelecom.ofdapp.databinding.ActivityMainBinding


/*
* Koin.
* Extension fun.
* Inline fun.
* Delegate (Shared prefs).
* Coroutines.
*/


class MainActivity : CoreActivity<ActivityMainBinding, MainVM>(MainVM::class) {

    override val vb: ActivityMainBinding
        get() = ActivityMainBinding.inflate(layoutInflater)

    override fun setContentViewBinding() {
        setContentView(vb.root)
    }

    override fun setupViews() {
        super.setupViews()
        vm.test()
    }
}