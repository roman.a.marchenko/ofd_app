package kg.nurtelecom.ofdapp.main

import kg.nurtelecom.core.CoreEvent
import kg.nurtelecom.core.view_model.CoreViewModel


abstract class MainVM : CoreViewModel() {
    abstract fun test()
}

class MainVmImpl constructor(private val config: Config) : MainVM() {
    override fun test() {
        sendEvent()
    }

    private fun sendEvent() {
        event.value = CoreEvent.Notification("URL: ${config.url}")
    }
}


data class Config(val url: String)