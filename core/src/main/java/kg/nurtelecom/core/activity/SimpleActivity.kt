package kg.nurtelecom.core.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class SimpleActivity<ViewBinding>: AppCompatActivity() {

    protected abstract val vb: ViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentViewBinding()
        parseDataFromIntent()
        setupViews()
    }

    open fun setupViews() {}

    open fun parseDataFromIntent() {}

    abstract fun setContentViewBinding()

}